package com.cvr.studentdataservices.builder;

import com.cvr.studentdataservices.model.Student;
import com.cvr.studentdataservices.web.resource.StudentResource;
import org.springframework.stereotype.Component;

@Component
public class StudentBuilder {

    public Student build(StudentResource resource) {
        Student student = new Student();
        student.setRollNo(resource.getRollNo());
        student.setName(resource.getName());
        student.setDepartment(resource.getDepartment());
        student.setSection(resource.getSection());
        return student;
    }
}
