package com.cvr.studentdataservices.web.assembler;

import com.cvr.studentdataservices.model.Student;
import com.cvr.studentdataservices.web.resource.StudentResource;
import org.springframework.stereotype.Component;

@Component
public class StudentAssembler {

    public StudentResource assemble(Student student) {
        StudentResource resource = new StudentResource();
        resource.setId(student.getId());
        resource.setRollNo(student.getRollNo());
        resource.setName(student.getName());
        resource.setDepartment(student.getDepartment());
        resource.setSection(student.getSection());
        return resource;
    }
}
