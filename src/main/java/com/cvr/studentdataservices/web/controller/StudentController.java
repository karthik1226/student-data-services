package com.cvr.studentdataservices.web.controller;

import com.cvr.studentdataservices.service.StudentService;
import com.cvr.studentdataservices.web.resource.StudentResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/students")
public class StudentController {

    @Autowired
    private StudentService studentService;

    @GetMapping("/{id}")
    public ResponseEntity<StudentResource> getStudent(@PathVariable("id") String id) {
        return new ResponseEntity<>(studentService.getStudent(id), HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<StudentResource> createStudent(@RequestBody StudentResource resource) {
        return new ResponseEntity<>(studentService.createStudent(resource), HttpStatus.CREATED);
    }
}
