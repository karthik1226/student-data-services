package com.cvr.studentdataservices.web.resource;

import lombok.Data;

@Data
public class StudentResource {

    private String id;
    private String rollNo;
    private String name;
    private String department;
    private String section;
}
