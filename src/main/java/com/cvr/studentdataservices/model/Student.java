package com.cvr.studentdataservices.model;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document
public class Student {

    private String id;
    private String rollNo;
    private String name;
    private String department;
    private String section;
}
