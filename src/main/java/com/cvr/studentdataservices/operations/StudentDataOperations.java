package com.cvr.studentdataservices.operations;

import com.cvr.studentdataservices.exception.StudentNotFoundException;
import com.cvr.studentdataservices.model.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.stereotype.Component;

@Component
public class StudentDataOperations {

    @Autowired
    private MongoOperations mongoOperations;

    public Student findStudentById(String id) {
        Student student = mongoOperations.findById(id, Student.class);
        if (student != null) {
            return student;
        }
        throw new StudentNotFoundException();
    }

    public Student createStudent(Student student) {
        return mongoOperations.insert(student);
    }
}
