package com.cvr.studentdataservices.service.impl;

import com.cvr.studentdataservices.builder.StudentBuilder;
import com.cvr.studentdataservices.model.Student;
import com.cvr.studentdataservices.operations.StudentDataOperations;
import com.cvr.studentdataservices.service.StudentService;
import com.cvr.studentdataservices.web.assembler.StudentAssembler;
import com.cvr.studentdataservices.web.resource.StudentResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class StudentServiceImpl implements StudentService {

    @Autowired
    private StudentDataOperations studentDataOperations;

    @Autowired
    private StudentAssembler studentAssembler;

    @Autowired
    private StudentBuilder studentBuilder;

    @Override
    public StudentResource getStudent(String id) {
        Student student = studentDataOperations.findStudentById(id);
        return studentAssembler.assemble(student);
    }

    @Override
    public StudentResource createStudent(StudentResource resource) {
        Student student = studentBuilder.build(resource);
        Student createdStudent = studentDataOperations.createStudent(student);
        return studentAssembler.assemble(createdStudent);
    }
}
