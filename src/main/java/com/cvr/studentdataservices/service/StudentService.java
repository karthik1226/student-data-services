package com.cvr.studentdataservices.service;

import com.cvr.studentdataservices.web.resource.StudentResource;
import org.springframework.stereotype.Service;

@Service
public interface StudentService {

    StudentResource getStudent(String id);

    StudentResource createStudent(StudentResource resource);
}
